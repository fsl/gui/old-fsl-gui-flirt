import { useState, useEffect, useRef, useCallback } from 'react'
import Box from '@mui/material/Box'
import LoadingButton from '@mui/lab/LoadingButton';
import { Button, FormControl, Select, MenuItem, FormHelperText, InputLabel, Card, CardContent, Slider, Input, Collapse, Snackbar, FormControlLabel, Checkbox, Switch } from '@mui/material'
import { Container, CssBaseline, TextField, Typography, Grid, Divider } from '@mui/material'
import { Niivue, NVImage } from '@niivue/niivue'
import { io } from "socket.io-client";
import "./App.css"

// get web socket connection info from url before rendering anything
let urlParams = new URLSearchParams(window.location.search)
let host = urlParams.get('host') || import.meta.env.VITE_FSLGUI_HOST || 'localhost'
let socketServerPort = urlParams.get('socketServerPort') || import.meta.env.VITE_FSLGUI_WS_PORT
let fileServerPort = urlParams.get('fileServerPort') || import.meta.env.VITE_FSLGUI_FS_PORT
console.log(host, socketServerPort, fileServerPort)

// different socket clients for different widgets
const inFileSocket = io(`ws://${host}:${socketServerPort}`)
const refFileSocket = io(`ws://${host}:${socketServerPort}`)
const runSocket = io(`ws://${host}:${socketServerPort}`)
const refWeightFileSocket = io(`ws://${host}:${socketServerPort}`)
const inputWeightFileSocket = io(`ws://${host}:${socketServerPort}`)



const nv = new Niivue()

async function addNiiVueImage(url, color='red', opacity=0.8){
	let img = await NVImage.loadFromUrl(url, '', color, opacity)
	nv.addVolume(img)
}

const NiiVue = ({url}) => {
	const canvas = useRef()
	useEffect(() => {
		nv.attachToCanvas(canvas.current)
		nv.loadVolumes([{url:url}])
	}, [url])

	return (
		<Grid container item xs={12} m={2} alignItems='center' justifyContent='center' spacing={0} direction='row'>
			<canvas ref={canvas} height={200} width={640}></canvas>
	</Grid>
	)
}

function InputField({text, updateOptsValue}) {
	return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='input file' 
					onInput={(e) => {updateOptsValue('-in', e.target.value)}}
					value={text}>
				</TextField>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'flex-end'}}>
				<Button 
					variant='contained' 
					style={{margin:0, marginLeft: 12}}
					onClick={()=>{inFileSocket.emit('files')}}
				>
					Choose
				</Button>
			</Grid>
		</Grid>
	)
}

function ReferenceField({text, updateOptsValue}) {
		return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='reference file' 
					onInput={(e) => {updateOptsValue('-ref', e.target.value)}}
					value={text}>
				</TextField>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'flex-end'}}>
				<Button 
					variant='contained' 
					style={{margin:0, marginLeft: 12}}
					onClick={()=>{refFileSocket.emit('files',{'standard': true})}}
				>
					Choose
				</Button>
			</Grid>
		</Grid>
	)
}


function Title({text}) {
	return (
		<Typography variant='h5' sx={{margin: '20px'}}>
			{text}
		</Typography>
	)
}

function OutputField({text, updateOptsValue}) {
	return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='output file'
					onInput={(e) => {updateOptsValue('-out', e.target.value)}}
					value={text}
				>
				</TextField>
			</Grid>
			{/*
			<Grid item xs={3} sx={{justifyContent: 'center'}}>
				<Button variant='contained' style={{margin:0, marginLeft: 12}}>Choose</Button>
			</Grid>
			*/}
		</Grid>
	)
}


function ActionButtons({handleMoreOptions, commandString, editedCommandString, isRunning, setIsRunning}) {
	let commandToRun = commandString
	if (commandString !== editedCommandString){
		commandToRun = editedCommandString // assume user wants to run their edited version
	}
	console.log(commandToRun)
	return (
		<Grid container item xs={12} alignItems='center' justifyContent='center' spacing={0} direction='row'>
			<LoadingButton
				onClick={()=>{runSocket.emit('run', {'run': commandToRun}); setIsRunning(true)}}
        loading={isRunning}
        loadingPosition="end"
        variant="contained"
				style={{margin:0}}
      >
        Run
      </LoadingButton>
			<Button style={{margin:0}} onClick={handleMoreOptions}>more options</Button>
		</Grid>
	)
}

function DOFselect({dofValue, updateOptsValue}) {
	return (
		<Grid item xs={9} alignItems='center' spacing={0}>
			<FormControl sx={{display: 'flex'}}>
				<InputLabel>Model/DOF</InputLabel>
				<Select
					value={dofValue}
					label="Model/DOF"
					onChange={(e)=>{updateOptsValue('-dof', e.target.value)}}
				>
					<MenuItem value={'3'}>(3 DOF) Translation only</MenuItem>
					<MenuItem value={'6'}>(6 DOF) Rigid body</MenuItem>
					<MenuItem value={'7'}>(7 DOF) Global rescale</MenuItem>
					<MenuItem value={'9'}>(9 DOF) Traditional</MenuItem>
					<MenuItem value={'12'}>(12 DOF) Affine</MenuItem>
				</Select>
				<FormHelperText>Input to reference</FormHelperText>
			</FormControl>
		</Grid>
	)
	
}

function InterpSelect({interpValue, updateOptsValue}) {
	return (
		<Grid item xs={12} alignItems='center' spacing={0}>
			<FormControl sx={{display: 'flex', m:2}}>
				<InputLabel>Interpolation</InputLabel>
				<Select
					value={interpValue}
					label="Interpolation"
					onChange={(e)=>{updateOptsValue('-interp', e.target.value)}}
				>
					<MenuItem value={'trilinear'}>Tri-linear</MenuItem>
					<MenuItem value={'nearestneighbour'}>Nearest neighbour</MenuItem>
					<MenuItem value={'sinc'}>Sinc</MenuItem>
					<MenuItem value={'spline'}>Spline</MenuItem>
				</Select>
				<FormHelperText>set the image interpolation method to use</FormHelperText>
			</FormControl>
		</Grid>
	)
	
}

function CostSelect({costValue, updateOptsValue}) {
	return (
		<Grid item xs={12} alignItems='center' spacing={0}>
			<FormControl sx={{display: 'flex', m:2}}>
				<InputLabel>Cost function</InputLabel>
				<Select
					value={costValue}
					label="Cost function"
					onChange={(e)=>{updateOptsValue('-cost', e.target.value)}}
				>
					<MenuItem value={'mutualinfo'}>Mutual Information</MenuItem>
					<MenuItem value={'corratio'}>Correlation Ratio</MenuItem>
					<MenuItem value={'normmi'}>Normalised Mutual Information</MenuItem>
					<MenuItem value={'normcorr'}>Normalised Correlation (intra-modal)</MenuItem>
					<MenuItem value={'leastsq'}>Least Squares (intra-modal)</MenuItem>
				</Select>
				<FormHelperText>set the desired cost function to use</FormHelperText>
			</FormControl>
		</Grid>
	)
}

function RefWeightField({text, updateOptsValue}) {
	return (
		<Grid container item xs={12} ml={2} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='reference weighting file' 
					onInput={(e) => {updateOptsValue('-refweight', e.target.value)}}
					value={text}>
				</TextField>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'flex-end'}}>
				<Button 
					variant='contained' 
					style={{margin:0, marginLeft: 12}}
					onClick={()=>{refWeightFileSocket.emit('files')}}
				>
					Choose
				</Button>
			</Grid>
		</Grid>
	)
}

function InputWeightField({text, updateOptsValue}) {
	
	return (
		<Grid container item xs={12} ml={2} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='input weighting file' 
					onInput={(e) => {updateOptsValue('-inweight', e.target.value)}}
					value={text}>
				</TextField>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'flex-end'}}>
				<Button 
					variant='contained' 
					style={{margin:0, marginLeft: 12}}
					onClick={()=>{inputWeightFileSocket.emit('files')}}
				>
					Choose
				</Button>
			</Grid>
		</Grid>
	)
}



function OptionsContainer({moreOptions, options, updateOptsValue}) {
	return (
		<Collapse in={moreOptions} timeout="auto" unmountOnExit>
			<Grid container item xs={12} spacing={2} direction='row'>
				<InterpSelect interpValue={options['-interp']}	updateOptsValue={updateOptsValue} />
				<CostSelect costValue={options['-cost']} updateOptsValue={updateOptsValue} />
				<RefWeightField text={options['-refweight']} updateOptsValue={updateOptsValue} />
				<InputWeightField text={options['-inweight']} updateOptsValue={updateOptsValue} />
			</Grid>
		</Collapse>
	)
}

function CommandStringPreview({commandString, setCommandString, editedCommandString, setEditedCommandString}) {
	function handleInput(e){
		console.log(e.target.textContent)
		setEditedCommandString(e.target.textContent)
	}
	return (
		<Grid 
			container 
			item 
			xs={12}
			alignItems='center'
			justifyContent='center' 
			spacing={0}
			direction='row'
		>
			<Typography
				sx={{'fontFamily': 'Monospace'}} 
				suppressContentEditableWarning={true} 
				contentEditable={true}
				onInput={handleInput}
			>
				{`${commandString}`}
			</Typography>
		</Grid>
	)
}

export default function Flirt() {
	useEffect(()=>{
		runSocket.on('run', (data) => {
			console.log('run', data)
			setIsRunning(false)
		})
	}, [])

	const title = "FLIRT"
  const defaultOpts = {
		'-in': 				'input', 		// input file path
		'-out': 			'output', 		// output file path
		'-ref': 			'reference',
		'-omat': 			'output matrix file',
		'-dof': 			'12',
		'-interp': 		'trilinear',
		'-cost': 			'corratio',
		'-inweight': 	null,
		'-refweight': null,
	}
	const [opts, setOpts] = useState(defaultOpts)
	// the boolean variable to show or hide the snackbar
	const [showSnackBar, setShowSnackBar] = useState(false)
	// the message, and message setter for the snackbar
	const [snackBarMessage, setSnackBarMessage] = useState('')
	const [moreOptions, setMoreOptions] = useState(false)
	const [commandString, setCommandString] = useState('')
	const [editedCommandString, setEditedCommandString] = useState('')
	const [isRunning, setIsRunning] = useState(false)
	const [niivueImage, setNiivueImage] = useState('')
	
	
	if (host === null || socketServerPort === null || fileServerPort === null){
		setSnackBarMessage('unable to contact backend application')
	}

	inFileSocket.on('files', (data) => {
		updateOptsValue('-in', data[0] ? data[0]: '')
		if (data[0] !== '') {
			setNiivueImage(`http://${host}:${fileServerPort}/file/?filename=${data[0]}`)
		}
	})
		
	refFileSocket.on('files', (data) => {
		updateOptsValue('-ref', data[0] ? data[0]: '')
	})

	inputWeightFileSocket.on('files', (data) => {
		updateOptsValue('-inweight', data[0] ? data[0]: '')
	})

	refWeightFileSocket.on('files', (data) => {
		updateOptsValue('-refweight', data[0] ? data[0]: '')
	})



	function handleSnackBarClose() {
		setShowSnackBar(false)
	}

	function showSnackBarIfMsg() {
		if (snackBarMessage !== '') {
			setShowSnackBar(true)
		}
	}
	// whenever the snackbar message changes, run the effect (e.g. show the snackbar to the user when the message is updated)
	useEffect(() => {showSnackBarIfMsg()}, [snackBarMessage])
	useEffect(() => {updateCommandString()}, [opts])

	function setOutputName(fileName) {
		if (!fileName) {
			return ''
		}
		let extIdx = fileName.lastIndexOf('.nii')
		if (extIdx < 0) {
			return fileName + '_flirt.nii.gz'
		} else {
			return fileName.substring(0, extIdx) + '_flirt.nii.gz'
		}
	}

	function setOmatName(fileName) {
		if (!fileName) {
			return ''
		}
		let extIdx = fileName.lastIndexOf('.nii')
		if (extIdx < 0) {
			return fileName + '_flirt.mat'
		} else {
			return fileName.substring(0, extIdx) + '_flirt.mat'
		}
	}


	function updateCommandString() {
		let command = 'flirt '
		for (let [key, value] of Object.entries({...opts})) {
			if (value !== null) {
				if (value === false){
					continue
				} else if (value === true) {
					// if true the just pass the key to set boolean bet values
					value = ''	
				}
				command += `${key} ${value} `
			}
		}
		setCommandString(command)
	}


	function handleMoreOptions() {
		setMoreOptions(!moreOptions)
	}


	function updateOptsValue(option, value) {
		setOpts(defaultOpts => ({
			...defaultOpts,
			...{[option]: value}
		}))
		if (option === '-in') {
			setOpts(defaultOpts => ({
				...defaultOpts,
				...{'-out': setOutputName(value)},
				...{'-omat': setOmatName(value)}
			}))
		}
		updateCommandString()
	}

  return (
		<Container component="main" style={{height: '100%'}}>
			<CssBaseline enableColorScheme />
			<Box sx={{
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				height: '100%'
				}}>
				<Title text={title} />
				<Grid container spacing={2}>
					<InputField
						updateOptsValue={updateOptsValue}
						text={opts['-in']}
					/>
					<DOFselect 
						updateOptsValue={updateOptsValue} 
						dofValue={opts['-dof']}
					/>
					<ReferenceField
						updateOptsValue={updateOptsValue}
						text={opts['-ref']}
					/>
					<OutputField 
						updateOptsValue={updateOptsValue}
						text={opts['-out']}
					/>
					<ActionButtons
						handleMoreOptions={handleMoreOptions}
						commandString={commandString}
						editedCommandString={editedCommandString}
						isRunning={isRunning}
						setIsRunning={setIsRunning}
					/>
					<OptionsContainer
						moreOptions={moreOptions}
						options={opts}
						updateOptsValue={updateOptsValue}
					/>
					<CommandStringPreview 
						commandString={commandString} 
						setCommandString={setCommandString}
						editedCommandString={editedCommandString}
						setEditedCommandString={setEditedCommandString}
					/>
					<NiiVue url={niivueImage}/>
					<Snackbar
						anchorOrigin={{horizontal:'center', vertical:'bottom'}}
						open={showSnackBar}
						onClose={handleSnackBarClose}
						message={snackBarMessage}
						key='betSnackBar'
						autoHideDuration={5000}
					/>
				</Grid>
			</Box>
		</Container>
  )
}
